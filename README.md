# React - Boilerplate

## Features
- React
- Redux
- React Router
- React Router Redux
- Redux Saga
- Redux Saga Routes
- Loadable (Code Splitting)
- Storybook

## TODO 
- Jest / Enzyme
- Unit tests
- ENV config file (Load config at runtime, not at compile time)
- Switch to NPM for auditing benefits
- Linting (Airbnb rules)?
- (Prettier)[https://github.com/prettier/prettier}]

## Application approach
- Routes
- View
- Logic
- State
- Persist

## React 


## Redux
### Implmentation of redux
Follows the ducks proposal, (find out more)[https://github.com/erikras/ducks-modular-redux]

## Routing / Redux Routing


## Sagas


## Code splitting / Performance


## CSS Methodology
- Boxmodel
- B.E.M.

## Animation

## Storybook


## Testing

## Configuration (TODO)
Create a config folder with each environment defined as ```properties.[ENV].json``` e.g. for dev environment it would be ```properties.dev.json```, when deployed the file will be renamed to ```properties.json```, this will be loaded by the application. This allows the configuration to be defined at an environment level, not during the bundling of the JS file. 
