const path = require('path');

module.exports = {
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        loaders: ["style-loader", "css-loader", "sass-loader"],
        include: path.resolve(__dirname, '../')
      },
      {
        test: /\.svg$/,
        loaders: ['file-loader'],
        include: path.resolve(__dirname, '../')
      },
      {
        test: /\.(jpg|png)$/,
        loaders: ['file-loader'],
        include: path.resolve(__dirname, '../')
      },
    ]
  }
}