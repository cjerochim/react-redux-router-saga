
import React from 'react';

const styles = {
  layout: {
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
};

const layoutCenter = (fn) => (
  <div style={styles.layout}>
    {fn()}      
  </div>
);

export default {
  layoutCenter,
}