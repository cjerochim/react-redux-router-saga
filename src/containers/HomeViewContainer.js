import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import HomeView from '../components/HomeView/HomeView';

const mapStateToProps = ({ home }) => {
  return {
    isActive: home.isBrandActive,
  };
}

export default connect(mapStateToProps, null)(HomeView);

