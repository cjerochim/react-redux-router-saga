import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TodoView from '../components/TodoView/TodoView';
import { addTask, updateTaskComplete } from '../redux/reducers/tasksReducer';

const mapStateToProps = ({ tasks: { list } }) =>
  ({ tasks: list });

const mapDispatchToProps = dispatch => bindActionCreators({
  onSubmit: addTask,
  onToggleComplete: updateTaskComplete,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TodoView);

