
import { router } from 'redux-saga-router';
import { delay } from 'redux-saga';

import { 
  put,
  all,
  call,
  fork,
  takeLatest,
} from 'redux-saga/effects';

import history from '../../services/historyService';

import {
  ADD_TASK,
  UPDATE_TASK_COMPLETE,
  setTask,
  setTaskComplete,
} from '../reducers/tasksReducer';

import {
  setBrandActive,
} from '../reducers/homeReducer';


function *updateTaskComplete({ payload: { id, isComplete } }) {
  yield put(setTaskComplete(id, isComplete));
}

function *addTask({ payload: { task } }) {
  yield put(setTask(Date.now().toString(), task, Date.now(), false));
}

function *routeRoot() {
  yield call(delay, 250);
  yield put(setBrandActive(true));
}

function *routeTodo() {
  yield put(setBrandActive(false));
}


const routes = [
  { pattern: '/', handler: routeRoot },
  { pattern: '/todo', handler: routeTodo },
];

export default function* routeSaga() {
    yield all([
      fork(router, history, routes),
      takeLatest(UPDATE_TASK_COMPLETE, updateTaskComplete),
      takeLatest(ADD_TASK, addTask),
    ]);
}