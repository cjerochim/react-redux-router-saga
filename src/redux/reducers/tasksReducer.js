import { merge, append, map, sortBy, prop, compose } from 'ramda';
import {
  mergeById,
} from '../../utils';

export const ADD_TASK = 'app/task/ADD_TASK';
export const UPDATE_TASK_COMPLETE = 'app/task/UPDATE_TASK_COMPLETE';
export const SET_TASK = 'app/task/SET_TASK';
export const SET_TASK_COMPLETE = 'app/task/SET_TASK_COMPLETE';

const initialState = {
  list: [],
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch(type) {
    case SET_TASK:
      return merge(state, {
        list: append(payload, state.list),
      });
    case SET_TASK_COMPLETE:
      return merge(state, {
        list: compose(
          sortBy(prop('created')),
          map(mergeById(payload.id, { isComplete: payload.isComplete })),
        )(state.list),
      });
      default:
      return state;
  }
}

export const addTask = task =>
  ({ type: ADD_TASK, payload: { task } });

  export const setTask = (id, title, created, isComplete = false) =>
  ({ type: SET_TASK, payload: { id, title, created, isComplete } });

export const updateTaskComplete = (id, isComplete = true) =>
  ({ type: UPDATE_TASK_COMPLETE, payload: { id, isComplete } });

export const setTaskComplete = (id, isComplete = true) =>
  ({ type: SET_TASK_COMPLETE, payload: { id, isComplete } });