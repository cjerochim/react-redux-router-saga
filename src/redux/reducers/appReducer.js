
import pkg from '../../../package.json';
import { merge } from 'ramda';

export const SET_REQUESTING = 'app/request/SET_REQUESTING';

const initialState = {
  version: pkg.version,
  isRequesting: false,
  error: {},
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch(type) {
    default:
      return state;
  }
};

export const setRequesting = isRequesting =>
  ({ type: SET_REQUESTING, payload: { isRequesting } });
