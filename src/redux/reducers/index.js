import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import appReducer from './appReducer';
import homeReducer from './homeReducer';
import taskReducer from './tasksReducer';

export default combineReducers({
  app: appReducer,
  home: homeReducer,
  tasks: taskReducer,
  router: routerReducer,
});