import { merge } from 'ramda';

export const SET_BRAND_ACTIVE = 'app/home/SET_BRAND_ACTIVE';

const initialState = {
  isBrandActive: false,
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_BRAND_ACTIVE:
      return merge(state, { isBrandActive: payload.isBrandActive });
    default:
      return state;
  }
};

export const setBrandActive = isBrandActive =>
  ({ type: SET_BRAND_ACTIVE, payload: { isBrandActive } });