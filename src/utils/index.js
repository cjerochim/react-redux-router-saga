import { merge, curry } from 'ramda';

const mergeById = curry((id, updateAttr, item) => {
  if (id === item.id) return merge(item, updateAttr);
  return item;
})


export {
  mergeById,
}