import React from 'react';
import PropTypes from 'prop-types';
import VersionContainer from '../../containers/VersionContainer';
import Nav from '../Nav/Nav';

import './App.scss';

const App = ({ children }) => (
  <div className="app">
    <Nav />
    {children}
    <div className="app__version">
      <VersionContainer />
    </div>
  </div>
)
 

export default App;