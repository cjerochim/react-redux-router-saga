import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import HomeViewLoadable from '../../Loadables/HomeViewLoadables';
import TodoViewLoadable from '../../loadables/TodoViewLoadables';

const NoPage = () => (<div />);

import './Routes.scss';

const Routes = ({ location }) => (
  <TransitionGroup className="routes">
    <CSSTransition key={location.key} timeout={500} classNames="routes-">
      <Switch location={location}>
        <Route exact path="/" component={HomeViewLoadable} />
        <Route path="/todo" component={TodoViewLoadable} />
        <Route path="*" component={NoPage} />
      </Switch>
    </CSSTransition>
  </TransitionGroup>
);

Routes.propTypes = {
  location: PropTypes.object.isRequired,
};

export default Routes;