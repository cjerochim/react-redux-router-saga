import React from 'react';
import PropTypes from 'prop-types';
import Brand from '../Brand/Brand';
import View from '../View/View';

const HomeView = ({ isActive }) => (
  <View center>
    <Brand isActive={isActive} />
  </View>
);

HomeView.defaultProps = {
  isActive: true,
};

HomeView.propTypes = {
  isActive: PropTypes.bool,
};

export default HomeView;