import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import View from '../View/View';
import TodoHeader from '../TodoHeader/TodoHeader';
import TodoList from '../TodoList/TodoList';

import './TodoView.scss';

const TodoView = ({ tasks, onSubmit, onToggleComplete }) => (
  <View center>
    <div className="todo-view">
      <TodoHeader onSubmit={onSubmit} />
      <TodoList tasks={tasks} onToggleComplete={onToggleComplete} />
    </div>
  </View>
);


TodoView.defaultProps = {
  list: [],
};

TodoView.propTypes = {
  tasks: PropTypes.array,
  onSubmit: PropTypes.func.isRequired,
  onToggleComplete: PropTypes.func.isRequired,
};

export default TodoView;