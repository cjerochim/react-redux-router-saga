import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { isValidTask } from '../../validation';
import './TodoHeader.scss';

class TodoHeader extends Component {
  constructor(props) {
    super(props);
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.state = {
      inputValue: '',
      isDisabled: true,
    };
  }

  componentDidMount() {
    if(this.input) {
      // Add delay to ensure focus does not effect animation fade in
      setTimeout(() => { this.input.focus() }, 200);
    }
  }

  onSubmitHandler(e) {
    e.preventDefault();
    const { onSubmit } = this.props;
    const { inputValue } = this.state;
    this.setState(state => ({ ...state, inputValue: '', isDisabled: true }));
    onSubmit(inputValue);
  }

  onChangeHandler({ target: { value } }) {
    const isValid = isValidTask.isValidSync(value);
    this.setState(state => ({ ...state, inputValue:value, isDisabled: !isValid }));    
  }

  render() {
    const { inputValue, isDisabled } = this.state;
    return (
      <header className="todo-header">
        <form className="todo-header__form"
          onSubmit={this.onSubmitHandler}
          autoComplete="off"
        >
          <div className="todo-header__group">
            <label className="todo-header__label" htmlFor="task">Add New Task</label>
            <input className="todo-header__input" ref={el => { this.input = el; }} type="text" value={inputValue} id="task" name="task" onChange={this.onChangeHandler} />
          </div>
          <div className="todo-header__group">
            <button className="todo-Header__btn" disabled={isDisabled}>Add</button>
          </div>
        </form>
      </header>
    );
  }
}

TodoHeader.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default TodoHeader;