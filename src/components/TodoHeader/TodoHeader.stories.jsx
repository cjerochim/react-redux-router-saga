import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import TodoHeader from './TodoHeader';
import decorators from '../../../.storybook/decorators';

import '../../assets/styles/screen.scss'

const stories = storiesOf('Todo/Todo Header', module);

stories.addDecorator(withKnobs);
stories.addDecorator(decorators.layoutCenter);
stories
  .add('Default', () => (
    <TodoHeader
      onSubmit={action('Submit Action')}
    />
  )
);