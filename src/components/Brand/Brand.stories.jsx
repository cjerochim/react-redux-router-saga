import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean } from '@storybook/addon-knobs/react';
import Brand from '../Brand/Brand';
import decorators from '../../../.storybook/decorators';

import '../../assets/styles/screen.scss'

const stories = storiesOf('Band', module);

stories.addDecorator(withKnobs);
stories.addDecorator(decorators.layoutCenter);
stories
  .add('Default', () => 
    <Brand
      isActive={boolean('Is Active', true)}
    />
  );