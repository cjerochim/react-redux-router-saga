import React from 'react';
import PropTypes from 'prop-types';
import { Spring } from 'react-spring';

import './Brand.scss';

const Brand = ({ isActive }) => {
  const rotation = isActive? '0deg' : '360deg';
  const scale = isActive? 1 : 0.5;
  const opacity = isActive? 1: 0;
  return (
    <Spring
      config={{ tension: 100, friction: 15 }}
      to={{ transform: `rotate(${rotation}) scale(${scale})`, opacity }}
    >
      {styles =>
        <h1 style={styles} className="brand">
          <span className="brand__visually-hidden">BCG Digital Ventures</span>
        </h1>
      }
    </Spring>
  );
}

Brand.defaultProps = {
  isActive: false,
};

Brand.propTypes = {
  isActive: PropTypes.bool,
};

export default Brand;