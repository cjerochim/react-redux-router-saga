import React from 'react';
import { NavLink } from 'react-router-dom';

import './Nav.scss';

const Nav = () => (
  <nav className="nav">
    <h2 className="nav__title">Global Navigation</h2>
    <ul className="nav__list">
      <li><NavLink exact to="/" className="nav__btn" activeClassName="nav__btn--active">Home</NavLink></li>
      <li><NavLink to="/todo" className="nav__btn" activeClassName="nav__btn--active">Todo</NavLink></li>
    </ul>
  </nav>
);

export default Nav;