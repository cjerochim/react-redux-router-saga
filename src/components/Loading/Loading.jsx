import React from 'react';
import View from '../View/View';


const Loader = () => (
  <View center>
    <div>Loading</div>
  </View>
);

export default Loader;