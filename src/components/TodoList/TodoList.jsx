import React from 'react';
import PropTypes from 'prop-types';
import TaskItem from './TodoListItem';
import classnames from 'classnames';
import './TodoList.scss';


const TodoList = ({ tasks, onToggleComplete }) => {
  // Create Lists
  const tasksCompleted = tasks.filter(task => task.isComplete);
  const tasksTodo = tasks.filter(task => !task.isComplete);
  
  const stateObj = {
    'todo-list--tasks-active':  tasksTodo.length > 0,
    'todo-list--tasks-completed': tasksCompleted.length > 0,
  };
  const classState = classnames('todo-list', stateObj);
  // Render list of tasks
  return (
    <main className={classState}>
      <section className="todo-list__group todo-list__group--tasks">
        <h2 className="todo-list__title">Todo</h2>
        <ul className="todo-list__list">
          {tasksTodo.map(task => <li key={task.id}><TaskItem {...task} onToggleComplete={onToggleComplete} /></li>)}
        </ul>
      </section>
      <section className="todo-list__group todo-list__group--completed"> 
        <h2 className="todo-list__title">Done</h2>
        <ul className="todo-list__list">
          {tasksCompleted.map(task => <li key={task.id}><TaskItem {...task} onToggleComplete={onToggleComplete} /></li>)}
        </ul>
      </section>
    </main>
  );
};

TodoList.defaultProps = {
  tasks: [],
};

TodoList.propTypes = {
  tasks: PropTypes.array,
  onToggleComplete: PropTypes.func.isRequired,
};

export default TodoList;