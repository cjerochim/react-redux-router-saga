import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './TodoListItem.scss';

const TodoListItem = ({ id, title, isComplete, onToggleComplete }) => {
  const classState = classnames('todo-list-item', { 'todo-list-item--complete': isComplete });
  return (
    <aside className={classState}>
      <div className="todo-list-item__group todo-list-item__group--primary">
        <h3 className="todo-list-item__title">{title}</h3>
      </div>
      <div className="todo-list-item__group todo-list-item__group--secondary">
        <button className="todo-list-item__btn" onClick={() => onToggleComplete(id, !isComplete)}><span>Complete</span></button>
      </div>
    </aside>
  );
}

TodoListItem.defaultProps = {
  description: '',
  isComplete: false,
};

TodoListItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  onToggleComplete: PropTypes.func.isRequired,
  description: PropTypes.string,
  isComplete: PropTypes.bool,

};


export default TodoListItem;