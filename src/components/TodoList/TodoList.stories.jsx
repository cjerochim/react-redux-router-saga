import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import TodoList from '../TodoList/TodoList';
import decorators from '../../../.storybook/decorators';

import '../../assets/styles/screen.scss'

const stories = storiesOf('Todo/Todo List', module);

stories.addDecorator(withKnobs);
stories.addDecorator(decorators.layoutCenter);
stories
  .add('Default', () => (
    <TodoList
      onToggleComplete={action('On Task Complete')}
    />
  )
);