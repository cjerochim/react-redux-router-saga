import { string } from 'yup';


const isValidTask = string().min(3).max(250).required();

export {
  isValidTask,
}